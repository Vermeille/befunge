CC=gcc
LIBS=-lcurses -std=c99
SRC=stack.c file.c cursor.c program.c

.SUFFIXES: .c .o
all:$(SRC:.c=.o) main.c
	$(CC) $(LIBS) $^ -o $@ -g

$(SRC):$(SRC:.c=.h)

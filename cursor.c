/*
** cursor.c for befunge
**
** Made by Guillaume "Vermeille" Sanchez
** Login   sanche_g <Guillaume.V.Sanchez@gmail.com>
**
** Started on  ven. 02 déc. 2011 21:35:32 CET Guillaume "Vermeille" Sanchez
** Last update dim. 04 déc. 2011 17:13:05 CET Guillaume "Vermeille" Sanchez
*/

#include <stdio.h>
#include <stdlib.h>

#include "cursor.h"

void move_cursor(struct cursor* c)
{
    switch (c->direction)
    {
        case UP:
            c->y--;
            break;
        case DOWN:
            c->y++;
            break;
        case LEFT:
            c->x--;
            break;
        case RIGHT:
            c->x++;
            break;
    }
}

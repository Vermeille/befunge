#pragma once

/**
 * @brief Directions of the cursor
 */
enum direction
{
    LEFT,
    UP,
    RIGHT,
    DOWN
};

/**
 * @brief The cursor struct
 */
struct cursor
{
    /**
     * @brief X position
     */
    int x;
    /**
     * @brief Y position
     */
    int y;
    /**
     * @brief Move direction
     */
    enum direction direction;
};

/**
 * @brief Move cursor one step in its current direction
 *
 * @param c the cursor to move
 */
void move_cursor(struct cursor* c);

/*
** file.c for befunge
**
** Made by Guillaume "Vermeille" Sanchez
** Login   sanche_g <Guillaume.V.Sanchez@gmail.com>
**
** Started on  ven. 02 déc. 2011 21:08:41 CET Guillaume "Vermeille" Sanchez
** Last update ven. 02 déc. 2011 21:12:55 CET Guillaume "Vermeille" Sanchez
*/

#include <stdio.h>
#include <stdlib.h>

#include "file.h"

void count_width_height(char* program, int* w, int* h)
{
    int i = 0, wmax = 0;
    *h = 0;
    *w = 0;
    while (program[i] != '\0')
    {
        if (program[i] == '\n')
        {
            ++(*h);
            if (*w > wmax)
                wmax = *w;
        }
        ++(*w);
    ++i;
    }
    *w = wmax;
}

char* read_whole_file(FILE* fp)
{
    fseek(fp, 0, SEEK_END);
    long len = ftell(fp);
    char* file = malloc((len+1)*sizeof (char));
    rewind(fp);
    fread(file, sizeof (char), len, fp);
    file[len] = '\0';
    return file;
}


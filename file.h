#pragma once

/**
 * @brief Mesure the size of the program grid by counting \n
 *
 * @param program the program in plain text format
 * @param w Out value of width
 * @param h Out value of height
 */
void count_width_height(char* program, int* w, int* h);

/**
 * @brief Reads the wole file
 *
 * @param fp the FILE* representing the file
 *
 * @return The string containing all the read file
 */
char* read_whole_file(FILE* fp);

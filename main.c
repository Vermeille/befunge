/*
** main.c for befunge
**
** Made by Guillaume "Vermeille" Sanchez
** Login   sanche_g <Guillaume.V.Sanchez@gmail.com>
**
** Started on  ven. 02 déc. 2011 14:14:10 CET Guillaume "Vermeille" Sanchez
** Last update jeu. 19 janv. 2012 13:35:01 CET Guillaume "Vermeille" Sanchez
*/

#include <stdio.h>
#include <stdlib.h>

#include <curses.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include "stack.h"
#include "file.h"
#include "program.h"

void print_title_bar(void)
{
    char* title = "BEFUNGE DYNAMIC INTERPRETER";

    for (int i = 0 ; i < COLS/2 - strlen(title)/2 ; ++i)
        printw(" ");
    printw("%s", title);
    for (int i = 0 ; i < COLS/2 - strlen(title)/2 ; ++i)
        printw(" ");
}

char** construct_program_table(char* file, int w, int h)
{
    char** grid = malloc(h*sizeof (char*));
    int tmp = 0;
    for (int i = 0 ; i < h ; ++i)
    {
        grid[i] = malloc(w*sizeof (char));
        int j = 0;
        while (file[tmp+j] != '\n')
        {
            grid[i][j] = file[tmp+j];
            ++j;
        }
        tmp += j+1;

        while (j < w)
        {
            grid[i][j] = ' ';
            ++j;
        }
    }
    return grid;
}

void print_program(WINDOW* p_buf, struct cursor cur, int w, int h, char** grid)
{
    for (int i = 0 ; i < h ; i++)
    {
        for (int j = 0 ; j < w ; j++)
        {
            if (i == cur.y && j == cur.x)
            {
                wattron(p_buf, COLOR_PAIR(1));
                wprintw(p_buf, "%c", grid[i][j]);
                wattroff(p_buf, COLOR_PAIR(1));
            }
            else
                wprintw(p_buf, "%c", grid[i][j]);
        }
        wprintw(p_buf, "\n");
    }
}

int main(int argc, char** argv)
{
    if (argc == 1)
    {
        printf("You need to give a befunge source file !");
        return 1;
    }
    srand(time(NULL));
    WINDOW* win = initscr();
    WINDOW* p_buf = newwin(20, 80, 2, 2);
    WINDOW* o_buf = newwin(20, 80, 2, 83);
    WINDOW* s_buf = newwin(20, 161, 23, 2);
    //wbkgdset(p_buf, COLOR_PAIR(2));
    start_color();
    init_pair(1, COLOR_BLACK, COLOR_WHITE);
    init_pair(2, COLOR_CYAN, COLOR_BLACK);
    attron(COLOR_PAIR(1));
    print_title_bar();
    attroff(COLOR_PAIR(1));
    //box(p_buf, COLOR_PAIR(1), COLOR_PAIR(1));
    box(o_buf, COLOR_PAIR(1), COLOR_PAIR(1));
    box(s_buf, COLOR_PAIR(1), COLOR_PAIR(1));
    wmove(p_buf, 2, 0);
    wmove(o_buf, 2, 2);
    wmove(s_buf, 2, 2);
    refresh();

    int w, h;
    FILE* fp = fopen(argv[1], "r");
    if (!fp)
    {
        printf("Cannot open file\n");
        exit(1);
    }
    char* file = read_whole_file(fp);
    count_width_height(file, &w, &h);
    char** grid = construct_program_table(file, w, h);
    //wbkgdset(p_buf, COLOR_PAIR(2));

    struct program p;
    p.cursor.x = 0;
    p.cursor.y = 0;
    p.cursor.direction = RIGHT;
    p.grid = grid;
    p.width = w;
    p.height = h;
    p.environnement = NULL;
    p.is_string_mode = 0;

    while(process(&p, o_buf))
    {
        refresh();
        print_program(p_buf, p.cursor, w, h, grid);
        wmove(p_buf, 2, 0);
        werase(s_buf);
        wmove(s_buf, 2, 2);
        box(s_buf, COLOR_PAIR(1), COLOR_PAIR(1));
        print_stack(s_buf, p.environnement);
        wrefresh(p_buf);
        wrefresh(o_buf);
        wrefresh(s_buf);
        usleep(500000);
        refresh();
    }

    refresh();
    free(file);
    getch();
    endwin();

    return 0;
}


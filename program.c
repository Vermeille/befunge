/*
** program.c for befunge
**
** Made by Guillaume "Vermeille" Sanchez
** Login   sanche_g <Guillaume.V.Sanchez@gmail.com>
**
** Started on  ven. 02 déc. 2011 22:17:14 CET Guillaume "Vermeille" Sanchez
** Last update sam. 07 janv. 2012 00:00:28 CET Guillaume "Vermeille" Sanchez
*/

#include <stdio.h>
#include <stdlib.h>

#include "program.h"
#include <curses.h>

int process(struct program* p, WINDOW* o_buf)
{
    if (p->cursor.x == -1)
        p->cursor.x = p->width;
    else if (p->cursor.x == p->width)
        p->cursor.x = 0;

    if (p->cursor.y == -1)
        p->cursor.y = p->height;
    else if (p->cursor.y == p->height)
        p->cursor.y = 0;

    if (p->grid[p->cursor.y][p->cursor.x] == '"')
    {
        p->is_string_mode = !p->is_string_mode;
        move_cursor(&p->cursor);
        return 1;
    }

    if (p->is_string_mode)
    {
        push(&p->environnement, p->grid[p->cursor.y][p->cursor.x]);
        move_cursor(&p->cursor);
        return 1;
    }

    switch ((int)p->grid[p->cursor.y][p->cursor.x])
    {
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            push(&p->environnement, p->grid[p->cursor.y][p->cursor.x]-'0');
            break;
        case '-':
        case '+':
        case '*':
        case '/':
        case '%':
            operation(&p->environnement, p->grid[p->cursor.y][p->cursor.x]);
            break;
        case '!':
            if (!pop(&p->environnement))
                push(&p->environnement, 1);
            else
                push(&p->environnement, 0);
            break;
        case '`':
            superior(&p->environnement);
            break;
        case '>':
            p->cursor.direction = RIGHT;
            break;
        case '<':
            p->cursor.direction = LEFT;
            break;
        case '^':
            p->cursor.direction = UP;
            break;
        case 'v':
            p->cursor.direction = DOWN;
            break;
        case '?':
            p->cursor.direction = rand() % 4;
            break;
        case '_':
            if (!pop(&p->environnement))
                p->cursor.direction = RIGHT;
            else
                p->cursor.direction = LEFT;
            break;
        case '|':
            if (!pop(&p->environnement))
                p->cursor.direction = DOWN;
            else
                p->cursor.direction = UP;
            break;
        case ':':
            duplicate(&p->environnement);
            break;
        case '\\':
            swap(&p->environnement);
            break;
        case '$':
            pop(&p->environnement);
            break;
        case '.':
            wprintw(o_buf, "%d", pop(&p->environnement));
            break;
        case ',':
            wprintw(o_buf, "%c", pop(&p->environnement));
            break;
        case '#':
            move_cursor(&p->cursor);
            break;
        case '~':
            push(&p->environnement, wgetch(o_buf));
            break;
        case '@':
            return 0;
            break;
        default:
            break;
    }
    move_cursor(&p->cursor);
    return 1;
}

void operation(struct stack** s, char op)
{
    int a = pop(s);
    int b = pop(s);

    int result;

    if (op == '-')
        result = b-a;
    else if (op == '+')
        result = a+b;
    else if (op == '/')
    {
        if (a == 0)
            ;
        else
            result = b/a;
    }
    else if (op == '*')
        result = a*b;
    else if (op == '%')
    {
        if (a == 0)
            ;
        else
            result = a % b;
    }

    //printf("operation %c\n\r", op);
    push(s, result);
}

void superior(struct stack** s)
{
    int a = pop(s);
    int b = pop(s);

    push(s, (b > a));
}

void duplicate(struct stack** s)
{
    int a = pop(s);
    push(s, a);
    push(s, a);
}

void swap(struct stack** s)
{
    int a = pop(s);
    int b = pop(s);
    push(s, b);
    push(s, a);
}

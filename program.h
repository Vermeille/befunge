#pragma once
#include <curses.h>

#include "cursor.h"
#include "stack.h"

/**
 * @brief This struct represents a Befunge program in execution
 */
struct program
{
    /**
     * @brief The cursor, or instruction pointer
     */
    struct cursor cursor;
    /**
     * @brief The program, as a bidimmensional array
     */
    char** grid;
    /**
     * @brief Number of columns of the program
     */
    int width;
    /**
     * @brief Number of rows in the program
     */
    int height;
    /**
     * @brief The environment : the stack (befunge is stack based)
     */
    stack* environnement;
    /**
     * @brief A flag to say if we are currently in execution mode or in string
     * mode
     */
    int is_string_mode;
};

/**
 * @brief Do mathematical operation
 *
 * @param p The environment
 * @param op '+', '-', '/', or '*'
 */
void operation(struct stack** p, char op);
/**
 * @brief Executes the current instruction
 *
 * @param p The program
 * @param o_buf The output window to use for write and read instructions
 *
 * @return a bool saying if the program is terminated or not.
 */
int process(struct program* p, WINDOW* o_buf);
/**
 * @brief the '`' instruction
 *
 * @param p the environment
 */
void superior(struct stack** p);
/**
 * @brief Duplicate the value on the top of the stack
 *
 * @param s The stack
 */
void duplicate(struct stack** s);
/**
 * @brief Swaps the 2 firsts values on the top of the stack
 *
 * @param s The stack.
 */
void swap(struct stack** s);

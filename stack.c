/*
** stack.c for befunge
**
** Made by Guillaume "Vermeille" Sanchez
** Login   sanche_g <Guillaume.V.Sanchez@gmail.com>
**
** Started on  ven. 02 déc. 2011 15:29:05 CET Guillaume "Vermeille" Sanchez
** Last update ven. 06 janv. 2012 23:08:34 CET Guillaume "Vermeille" Sanchez
*/

#include <stdio.h>
#include <stdlib.h>

#include "stack.h"

/* s : pointeur sur pointeur sur pile
 * *s : pointeur sur pile
 * **s : pile
 */
int pop(stack** s)
{
    if (*s == NULL)
        return 0;
    stack* top = (*s)->next;
    int v = (*s)->value;
    free(*s);
    *s = top;
    return v;
}

void push(stack** s, int val)
{
    stack* node = malloc(sizeof (stack));
    node->next = *s;
    node->value = val;
    *s = node;
}

void print_stack(WINDOW* s_buf, stack* s)
{
    if (s == NULL)
    {
        wprintw(s_buf, "[EOS");
        return;
    }
    print_stack(s_buf, s->next);
    if (s->value > 40)
        wprintw(s_buf, " <- (%d, %c)", s->value, s->value);
    else
        wprintw(s_buf, " <- (%d)", s->value);
    //*/
}

stack* new_stack(void)
{
    return NULL;
}



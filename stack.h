#pragma once

#include <curses.h>

/**
 * @brief The stack represented as a linked list.
 */
typedef struct stack
{
    /**
     * @brief Pointer to the next element
     */
    struct stack* next;
    /**
     * @brief The value in this node
     */
    int value;
} stack;

/**
 * @brief Creates a new stack
 *
 * @return A pointer to a new void stack
 */
stack* new_stack(void);
/**
 * @brief Pushes an element on the top of the stack
 *
 * @param s A pointer to the stack
 * @param val the value to push
 */
void push(stack** s, int val);
/**
 * @brief Pops the value on the top of the stack. Return 0 if the stack is void
 *
 * @param s the stack
 *
 * @return the poped value
 */
int pop(stack** s);
/**
 * @brief Prints the stack
 *
 * @param s_buf the output buffer to print the stack on
 * @param s the stack
 */
void print_stack(WINDOW* s_buf, stack* s);

